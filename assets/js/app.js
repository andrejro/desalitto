jQuery(function ($) {
    //NameSpace
    var mainApp = mainApp || {};
    // DOM Ready
    $(function () {
        // Main controller
        mainApp.controller = {
            init: function () {
                // Common
                mainApp.menu.init();
                mainApp.tabs.init();
                mainApp.home.init();
                mainApp.product.init();
                // Resize
                $(window).trigger('resize');
            }
        };
        // Menu
        mainApp.menu = {
            init: function () {
                $('div.menu-toggle').on('click', function () {
                    $('body').toggleClass('menu-toggle-open');
                });
            }
        };
        // Tabs
        mainApp.tabs = {
            init: function () {
                $('.tabs-block-links-ul a').on('click', function () {
                    $('.tabs-block-links-ul a, .tabs-block-content-item').removeClass('active');
                    $(this).addClass('active');
                    var tabID = $(this).attr('href');
                    $(tabID).addClass('active');
                    return false;
                });
            }
        };
        // Home page
        mainApp.home = {
            init: function () {
                mainApp.home.mainSlider();
                mainApp.home.mainFilter();
                mainApp.home.mainSearch();
                mainApp.home.mainScrolltop();
            },
            mainSlider: function () {
                $('.home-carousel').owlCarousel({
                    loop: true,
                    margin: 0,
                    nav: true,
                    dots: false,
                    items: 1,
                    navText: ["<span class='icon-arr_left'></span>", "<span class='icon-arr_right'></span>"]
                });
                $('.home-carousel-2').owlCarousel({
                    loop: true,
                    margin: 0,
                    nav: true,
                    dots: true,
                    items: 2,
                    navText: ["<span class='icon-arr_left'></span>", "<span class='icon-arr_right'></span>"]
                });
                $('.slider-bottom').owlCarousel({
                    loop: true,
                    margin: 20,
                    nav: false,
                    dots: false,
                    items: 3,
                    navText: ["<span class='icon-arr_left'></span>", "<span class='icon-arr_right'></span>"],
                    responsive:{
                        0:{
                            items:1.2,
                            nav:false,
                            dots: true,
                        },
                        767:{
                            items:2,
                            nav:false,
                            dots: true,
                        },
                        920:{
                            items:3,
                            nav:false
                        }
                    }
                });
                var owl = $('.responsive-slider'),
                    owlOptions = {
                        loop: true,
                        margin: 20,
                        nav: false,
                        dots: false,
                        items: 3,
                        navText: ["<span class='icon-arr_left'></span>", "<span class='icon-arr_right'></span>"],
                        responsive: {
                            0: {
                                items: 1.2,
                                nav: false,
                                dots: true,
                            },
                            767: {
                                items: 2,
                                nav: false,
                                dots: true,
                            },
                            920: {
                                items: 3,
                                nav: false
                            }
                        }
                    };
                if ( $(window).width() < 854 ) {
                    var owlActive = owl.owlCarousel(owlOptions);
                } else {
                    owl.addClass('off');
                }
                $(window).resize(function() {
                    if ( $(window).width() < 854 ) {
                        if ( $('.owl-carousel').hasClass('off') ) {
                            var owlActive = owl.owlCarousel(owlOptions);
                            owl.removeClass('off');
                        }
                    } else {
                        if ( !$('.owl-carousel').hasClass('off') ) {
                            owl.addClass('off').trigger('destroy.owl.carousel');
                            owl.find('.owl-stage-outer').children(':eq(0)').unwrap();
                        }
                    }
                });
            },
            mainSearch: function (){
                $(document).on('click', '.search-button', function () {
                    $(this).parent().parent().toggleClass('active');
                });
            },
            mainFilter: function () {
                $(document).on('click', '.filter-block-title i, .filter-block-title .icon-arrow', function () {
                    $(this).closest('.filter-block').toggleClass('__opened');
                });
                $(document).on('click', '.__resetfilter', function () {
                    $(this).closest('.filter-block').find('input').prop('checked', false);
                    return false;
                });
            },
            mainScrolltop: function () {
                if ($('#back-to-top').length) {
                    var scrollTrigger = 100,
                        backToTop = function () {
                            var scrollTop = $(window).scrollTop();
                            if (scrollTop > scrollTrigger) {
                                $('#back-to-top').addClass('show');
                            } else {
                                $('#back-to-top').removeClass('show');
                            }
                        };
                    backToTop();
                    $(window).on('scroll', function () {
                        backToTop();
                    });
                    $('#back-to-top').on('click', function (e) {
                        e.preventDefault();
                        $('html,body').animate({
                            scrollTop: 0
                        }, 700);
                    });
                }
            },
        };

        // Product page
        mainApp.product = {
            init: function () {
                mainApp.product.prodSlider();
                mainApp.product.mainSlider();
                mainApp.product.mainQty();
                mainApp.product.optQty();
            },
            prodSlider: function () {
                $('.product-block-carousel').owlCarousel({
                    loop: true,
                    margin: 15,
                    nav: true,
                    dots: false,
                    items: 4,
                    navText: ["<span class='icon-arr_left'></span>", "<span class='icon-arr_right'></span>"],
                    responsive: {
                        0: {
                            items: 1,
                            nav: false,
                            dots: true
                        },
                        480: {
                            items: 2,
                        },
                        768: {
                            items: 3,
                        },
                        992: {
                            items: 4,
                        }
                    }
                })
            },
            mainSlider: function () {
                $('.product-main-carousel').owlCarousel({
                    thumbs: true,
                    thumbsPrerendered: true,
                    nav: false,
                    dots: false,
                    items: 1,
                    navText: ["<span class='icon-arr_left'></span>", "<span class='icon-arr_right'></span>"],
                })
            },
            mainQty: function () {
                $('.cardtable-qty .minus').click(function () {
                    var $input = $(this).parent().find('input');
                    var count = parseInt($input.val()) - 1;
                    count = count < 1 ? 1 : count;
                    $input.val(count);
                    $input.change();
                    return false;
                });
                $('.cardtable-qty .plus').click(function () {
                    var $input = $(this).parent().find('input');
                    $input.val(parseInt($input.val()) + 1);
                    $input.change();
                    return false;
                });
            },
            optQty: function () {
                $('.opt-select-input .minus').click(function () {
                    var $input = $(this).parent().find('input');
                    var count = parseInt($input.val()) - 1;
                    count = count < 1 ? 0 : count;
                    $input.val(count);
                    $input.change();
                    if(count>0)
                    {
                        $(this).closest('.opt-select-item').addClass('checkeds');
                    } else{
                        $(this).closest('.opt-select-item').removeClass('checkeds');
                    }
                    return false;
                });
                $('.opt-select-input .plus').click(function () {
                    var $input = $(this).parent().find('input');
                    var count = parseInt($input.val()) + 1;
                    $input.val(count);
                    $input.change();
                    if(count>0)
                    {
                        $(this).closest('.opt-select-item').addClass('checkeds');
                    } else{
                        $(this).closest('.opt-select-item').removeClass('checkeds');
                    }
                    return false;
                });
                $('a.reset-opt-link').on('click', function () {
                    $('.opt-select-input input').val(0);
                    $('.opt-select-item').removeClass('checkeds');
                    return false;
                });

            },
        };


        // Start app
        'function' == typeof mainApp.controller.init && mainApp.controller.init();
    }); // End DOM Ready
});