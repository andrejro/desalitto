var gulp = require('gulp'),
	prefix = require('gulp-autoprefixer'),
	livereload = require('gulp-livereload'),
	connect = require('gulp-connect'),
	csscomb = require('gulp-csscomb'),
	sass = require('gulp-sass'),
	plumber = require('gulp-plumber'),
	rename = require('gulp-rename'),
	minifycss = require('gulp-minify-css'),
	notify = require('gulp-notify');
	var sourcemaps = require('gulp-sourcemaps');


// SERVER
gulp.task('connect', function() {
	connect.server({
		root: [__dirname],
		livereload: true
	});
});

// CSS
gulp.task('css', function() {
	gulp.src('assets/scss/style2.scss')
		.pipe(plumber())
        .pipe(sass({indentedSyntax: false}))
		.pipe(prefix({ 
			browsers: ['last 2 versions', 'ie >= 9', 'Opera >= 12', 'Safari > 3']
		}))
		.pipe(csscomb())
		.pipe(sourcemaps.init())
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('assets/css'))
		.pipe(rename({suffix: '.min'}))
		.pipe(minifycss())
		.pipe(gulp.dest('assets/css'))
//		.pipe(notify('Done!'))
		.pipe(connect.reload());
});

// HTML
gulp.task('html', function() {
	gulp.src('*.html')
		.pipe(connect.reload());
});

// WATCH
gulp.task('watch', function() {
	gulp.watch(['assets/scss/*/*.scss', 'assets/scss/*.scss'], ['css']);
	gulp.watch('*.html', ['html']);
});

// DEFAULT
gulp.task('default', ['connect', 'html', 'css', 'watch']);